<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 */
class Fishing
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $length;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Place", inversedBy="fishing")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false, unique=true)
     */
    private $place;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image", mappedBy="fishing")
     */
    private $images;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="fisheries")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fish", inversedBy="fisheries")
     * @ORM\JoinColumn(name="fish_id", referencedColumnName="id")
     */
    private $fish;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Bait", inversedBy="fisheries")
     * @ORM\JoinColumn(name="bait_id", referencedColumnName="id")
     */
    private $bait;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Fishing
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return Fishing
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set length
     *
     * @param integer $length
     * @return Fishing
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return integer 
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set place
     *
     * @param \AppBundle\Entity\Place $place
     * @return Fishing
     */
    public function setPlace(\AppBundle\Entity\Place $place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \AppBundle\Entity\Place 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Add images
     *
     * @param \AppBundle\Entity\Image $images
     * @return Fishing
     */
    public function addImage(\AppBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AppBundle\Entity\Image $images
     */
    public function removeImage(\AppBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Fishing
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set fish
     *
     * @param \AppBundle\Entity\Fish $fish
     * @return Fishing
     */
    public function setFish(\AppBundle\Entity\Fish $fish = null)
    {
        $this->fish = $fish;

        return $this;
    }

    /**
     * Get fish
     *
     * @return \AppBundle\Entity\Fish 
     */
    public function getFish()
    {
        return $this->fish;
    }

    /**
     * Set bait
     *
     * @param \AppBundle\Entity\Bait $bait
     * @return Fishing
     */
    public function setBait(\AppBundle\Entity\Bait $bait = null)
    {
        $this->bait = $bait;

        return $this;
    }

    /**
     * Get bait
     *
     * @return \AppBundle\Entity\Bait 
     */
    public function getBait()
    {
        return $this->bait;
    }
}
