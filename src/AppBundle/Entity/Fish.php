<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FishRepository")
 */
class Fish
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Fishing", mappedBy="fish")
     */
    private $fisheries;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fisheries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Fish
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Fish
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add fisheries
     *
     * @param \AppBundle\Entity\Fishing $fisheries
     * @return Fish
     */
    public function addFishery(\AppBundle\Entity\Fishing $fisheries)
    {
        $this->fisheries[] = $fisheries;

        return $this;
    }

    /**
     * Remove fisheries
     *
     * @param \AppBundle\Entity\Fishing $fisheries
     */
    public function removeFishery(\AppBundle\Entity\Fishing $fisheries)
    {
        $this->fisheries->removeElement($fisheries);
    }

    /**
     * Get fisheries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFisheries()
    {
        return $this->fisheries;
    }
}
