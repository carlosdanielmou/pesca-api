<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"unsigned":true})
     */
    private $path;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fishing", inversedBy="images")
     * @ORM\JoinColumn(name="fishing_id", referencedColumnName="id")
     */
    private $fishing;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Image
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set fishing
     *
     * @param \AppBundle\Entity\Fishing $fishing
     * @return Image
     */
    public function setFishing(\AppBundle\Entity\Fishing $fishing = null)
    {
        $this->fishing = $fishing;

        return $this;
    }

    /**
     * Get fishing
     *
     * @return \AppBundle\Entity\Fishing 
     */
    public function getFishing()
    {
        return $this->fishing;
    }
}
